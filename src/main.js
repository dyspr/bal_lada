var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var lada = []
var dimension = {
  x: 31,
  y: 21
}
var initSize = 0.0275
var array = create2DArray(dimension.x, dimension.y, 0, true)
var array2 = create2DArray(dimension.x, dimension.y, 0, true)

function preload() {
  for (var i = 0; i < 2; i++) {
    lada.push(loadImage('img/fortepan_73544_' + i + '.jpg'))
  }
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      array[i][j] = lada[0].get(Math.floor(lada[0].width / dimension.x) * i, Math.floor(lada[0].width / dimension.x) * j, Math.floor(lada[0].height / dimension.y), Math.floor(lada[0].height / dimension.y))
    }
  }
  for (var i = 0; i < array2.length; i++) {
    for (var j = 0; j < array2[i].length; j++) {
      array2[i][j] = lada[1].get((lada[(i + j) % 2].width / dimension.x) * i, (lada[(i + j) % 2].width / dimension.x) * j, (lada[(i + j) % 2].height / dimension.y), (lada[(i + j) % 2].height / dimension.y))
    }
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension.x; i++) {
    for (var j = 0; j < dimension.y; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension.x * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimension.y * 0.5)) * boardSize * initSize)
      image(array2[i][j],
        -boardSize * initSize * 0.5 * abs(cos(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25)))),
        -boardSize * initSize * 0.5 * abs(cos(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25)))),
        boardSize * initSize * abs(cos(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25)))),
        boardSize * initSize * abs(cos(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25))))
      )
      image(
        array[i][j],
        -boardSize * initSize * 0.5 * abs(sin(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25)))),
        -boardSize * initSize * 0.5 * abs(sin(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25)))),
        boardSize * initSize * abs(sin(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25)))),
        boardSize * initSize * abs(sin(frameCount * 0.05 + Math.PI * 0.25 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x * 0.25))))
      )
      image(array2[i][j],
        -boardSize * initSize * 0.5 * (cos(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x)))),
        -boardSize * initSize * 0.5 * (cos(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x)))),
        boardSize * initSize * (cos(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x)))),
        boardSize * initSize * (cos(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x))))
      )
      image(
        array[i][j],
        -boardSize * initSize * 0.5 * (sin(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x)))),
        -boardSize * initSize * 0.5 * (sin(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x)))),
        boardSize * initSize * (sin(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x)))),
        boardSize * initSize * (sin(frameCount * 0.025 + ((i - Math.floor(dimension.x * 0.5))) * (1 / (dimension.x))))
      )
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
